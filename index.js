const express = require("express");
const bodyParser = require("body-parser");
const session = require('express-session');
const fs = require('fs');
const MemoryStore = require('memorystore')(session);

const levels = require("./levels.json");
const CONFIG = require("./config.json");
const key = 'jiilhogvfeebdectbkaa';

const app = express();
const sessionStore = new MemoryStore({
  checkPeriod: 10*60*1000 // prune expired entries every 24h
});

const activeAccounts = [];

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(express.static("frontend"));
app.use(session({
  secret: key,
  resave: false,
  rolling: false,
  saveUninitialized: false,
  store: sessionStore,
  cookie: {
    secure: false,
    maxAge: 10*60*1000, // 10 minutes
  },
  // genid: function(req) {
  //   return req.params.uid || "_000__tester__000";
  // }
}));


///// Registration
app.post("/register/:uid", async (req, res)=>{
  const accounts = getAccounts();
  console.log(accounts);
  const account = accounts.filter(a=>a[0]==req.params.uid)[0];
  if(account && account[1] == req.body.passwd){
    if(!account[2]){ // If is not tester acc
      const sessions = await new Promise((resolve)=>sessionStore.all((e,s)=>{resolve(s);}));
      if(Object.keys(sessions).some((session)=>sessions[session].login==req.params.uid)){
        return res.json(false);
      }
    }
    // Save session
    req.session.level = CONFIG.defaultLevel || 0;//levels.length-1;
    req.session.login = req.params.uid;
    req.session.account = account;
    req.session.points = 0;
    req.session.levels = [];
    req.session.save();

    if(!account[2]) saveData(req.session);


    res.json(true);
  }
  else{
    res.json(false);
  }
});


///// Loading levels
app.get("/getLevel", (req, res)=>{
  if(!req.session || !("level" in req.session)){
    return res.status(403).json("REGISTRATION_REQUIRED");
  }
  if(req.session.level == levels.length) {
    const points = req.session.points;
    req.session.destroy();
    return res.json(["end", points]);
  }
  const level = levels[req.session.level];

  res.json({
    field: level.field,
    limit: level.progLimit || false,
    procedures: level.procedures || 0,
    hint: level.hint || false,
    numLevels: levels.length,
    number: req.session.level+1,
    level: level.level,
    points: level.points,
    difficulty: level.difficulty
  });
});


///// Integrity helper
app.get("/getKey", (req, res)=>{
  if(!("level" in req.session)) return res.sendStatus(403);
  res.json(key);
});

///// Cookie time
app.get("/getTimeremaining", (req, res)=>{
  if(!("level" in req.session)) return res.sendStatus(403);
  res.json(req.session.cookie.expires);
});

///// Completing levels
app.post("/doneLevel", (req, res)=>{
  if(!req.session || !req.session.hasOwnProperty("level")){
    return res.status(403).json("REGISTRATION_REQUIRED");
  }
  if(req.session.level == levels.length) return res.sendStatus(404);
  const level = levels[req.session.level];
  let playerPos, holePos;


  // Get info about level
  level.field.forEach((row,y)=>{
    row.forEach((col,x)=>{
      if(col == "player"){
        playerPos = [x, y];
      }else if(col == "worm_hole"){
        holePos = [x, y];
      }
    });
  });


  // Check parameters
  if(
    !(
      ("moves" in req.body) &&
      !isNaN(req.body.moves)
    ) ||
    !(
      ("finalPos" in req.body) &&
      Array.isArray(req.body.finalPos) &&
      req.body.finalPos.length == 2
    ) ||
    !(
      ("posHistory" in req.body) &&
      Array.isArray(req.body.posHistory)
    ) ||
    !("key" in req.body)
  ){
    return res.status(400).json("INVALID_PARAMETERS");
  }


  // Convert parameters
  try{
    var moves = Number(req.body.moves);
  }catch(e){
    return res.status(500).json("DATA_CHECK_FAILED");
  }


  // Check integrity
  if(
    moves < level.safetyChecks.minMoves ||
    req.body.posHistory.length != moves ||
    req.body.finalPos[0] != holePos[0] ||
    req.body.finalPos[1] != holePos[1] ||
    req.body.key != key
  ){
    return res.status(400).json("DATA_INTEGRITY_DISPUTED");
  }


  // Check moves
  if(req.body.posHistory.some((pos)=>{
    if(level.field[pos[1]][pos[0]].startsWith("obstacle_")) return true;
    return false;
  })){
    return res.status(400).json("DATA_INTEGRITY_DISPUTED");
  }


  // All is good, proceed <3
  req.session.points += level.points;
  req.session.levels.push(level.points);
  req.session.level++;
  if(!req.session.account[2]) saveData(req.session);
  res.json(true);
});


app.all("/api/results/:method?", (req,res)=>{
  const shouldObject = req.params.method && req.params.method.startsWith("by");
  const objectId = req.params.method && req.params.method.replace(/^by/, "").toLowerCase();
  const accounts = getAccounts();
  const results = shouldObject ? {} : [];

  accounts.forEach((account,i)=>{
    if(account[2]) return;
    const result = {};

    result.login = account[0] || "None"+i;
    result.id = account[3] || "None"+i;
    result.order = account[4] || "None"+i;
    result.name = account[5] || "None"+i;
    result.score = 0;
    result.levels = [];

    if(fs.existsSync(".data/results/"+account[0]+".json")){
      const data = JSON.parse(String(fs.readFileSync(".data/results/"+account[0]+".json")));
      result.score = data.points;
      result.levels = data.levels;
    }

    if(shouldObject){
      if(!(objectId in result) || typeof result[objectId] === "object") return;
      const key = result[objectId] || "None"+i;
      results[key] = result;
    }else{
      const len = results.push(result);
    }
  });

  return res.json(results);
});


app.listen(CONFIG.port, CONFIG.host, ()=>{
  console.log("Listening");
});



function getAccounts() {
  return [
    ...JSON.parse(String(fs.readFileSync(".data/accounts.json"))),
    ...parseAccountsd()
  ];
}


function parseAccountsd() {
  const files = fs.readdirSync(".data/accounts.d/");
  const accounts = [];

  files.forEach((file) => {
    if(file.endsWith(".csv")){
      let data = (String(fs.readFileSync(".data/accounts.d/"+file))
        .split("\n")
        .map(a=>a
          .split(",")
          .map(a=>a.replace(/\"/g, "").trim())
        ).filter(a=>a!="")
      );
      const header = data.shift();
      if(header.length > 7){
        data.forEach((line) => {
          line = line.filter(a=>a!="");
          if(line.length < 8 || line[2].length < 2) return;
          accounts.push([line[3], line[2], false, line[0], isNaN(line[1]) ? line[1] : Number(line[1]), line[4]+" "+line[5]]);
        });
      }else if(header.length == 4){
        data.forEach((line) => {
          line = line.filter(a=>a!="");
          if(line.length != 4) return;
          accounts.push([line[2], line[1], true, -1, line[0], line[3]]);
        });
      }
    }
    else if (file.endsWith(".json")) {
      accounts.push(...JSON.parse(String(fs.readFileSync(".data/accounts.d/"+file))));
    }
  });

  return accounts;
}



function saveData(session) {
  return fs.writeFileSync(".data/results/"+session.login+".json", JSON.stringify({
    points: session.points,
    levels: session.levels
  }));
}
