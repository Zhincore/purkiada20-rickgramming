$(document).ready(()=>{
  var game;
  var key;
  var time = new Date();
  var timeInterval;

  getLevel();

  function getLevel(){
    syncTime();

    $.get("/getLevel", (levelData)=>{
      $.get("/getKey", (res)=>{
        key = res;
      });

      if(levelData[0] == "end"){
        $("#var-points").text(levelData[1]);
        $("#end-overlay").fadeIn("fast");
      }else{
        game = new Game(levelData, nextLevel);

        $("#info-level").text(levelData.level +"/"+ (levelData.numLevels) +" | "+ (levelData.difficulty));
        $("#points-level").text("Za "+levelData.points+" bodů");
      }

    }).fail((data)=>{
      if(data.responseJSON == "REGISTRATION_REQUIRED"){
        $("#reg-overlay").fadeIn("fast");
        $("#reg-form").submit(()=>{
          $("#reg-error").text("");
          $.post("/register/"+$("#reg-name").val(), {
            passwd: $("#reg-passwd").val()
          }, (res)=>{
            if(res === true){
              $("#reg-overlay").fadeOut();
              getLevel();
            }else{
              $("#reg-error").html("<div>Registrace selhala.<br>Zkontroluj, zda jsi zadal správné údaje.<br>Je možné, že jsi přihlášen v jiném prohlížeči,<br>v takovém případě to zkus znovu za 10 minut nebo použij předchozí prohlížeč.</div>")
            }
          }, "json");
        });
      }else{
        console.error(data.responseJSON);
      }
    });
  }

  function nextLevel(){
    $.post("/doneLevel", {
      moves: game.moves,
      finalPos: game.currentPos,
      posHistory: game.posHistory,
      key: key
    }, (res)=>{
      if(res === true){
        getLevel();
      }else{
        alert(res);
      }
    }).fail((res)=>{
      alert(res.responseJSON);
    }).always(syncTime);

    game.destroy();
    game = null;
    return;
  }

  $(window).resize(()=>{
    if(game){
      game.render();
    }
  });

  function syncTime(){
    $.get("/getTimeremaining", (expires)=>{
      time = new Date(expires);

      if(timeInterval) clearInterval(timeInterval);

      timeInterval = setInterval(()=>{
        var remains = new Date(time.getTime() - Date.now());

        if(remains.getTime() < 1) {
          clearInterval(timeInterval);
          timeInterval = null;

          $("#fail-overlay").fadeIn();
          game.destroy();
          game = null;
          return
        }

        $("#info-time").text(
          remains.getMinutes() +
          ":"+
          leadingZeros(remains.getSeconds())
        );
      }, 500);

    }).fail(()=>{
      setTimeout(syncTime, 500)
    });
  }
});
