var ASSETS = {
  player: `<img src="/img/ship.png" class="view-asset view-asset-bobbing" style="z-index: 100">`,
  obstacle_asteroid: `<img src="/img/asteroid.png" class="view-asset-big view-asset-floating">`,
  worm_hole: `
    <video width="auto" height="250px" class="view-asset-big view-asset-floating" style="animation-delay:-3s; z-index: 0;" poster="/img/worm_hole.png" autoplay loop>
      <source src="/vid/worm_hole.webm" type="video/webm; codec='vp9'">
      <source src="/vid/worm_hole.mov" type="video/quicktime; codec='qtrle'">
      <source src="/vid/worm_hole.avi" type="video/avi; codec='huffyuv'">
      <source src="/vid/worm_hole.mp4" type="video/mp4; codec='png'">
      <p>Načítání selhalo</p>
    </video>
  `,
  portal: `
    <video width="auto" height="250px" class="view-asset-small view-asset-portal" poster="/img/portal.png" autoplay loop>
      <source src="/vid/portal.webm" type="video/webm; codec='vp9'">
      <p>Načítání selhalo</p>
    </video>
  `,
  explosion: `
    <video width="auto" height="250px" class="view-asset-big" preload="" poster="/img/explosion.png">
      <source src="/vid/explosion.webm" type="video/webm; codec='vp9'">
      <p>Načítání selhalo</p>
    </video>
  `,
};
