function gcd(x, y) {
  if ((typeof x !== 'number') || (typeof y !== 'number'))
    return false;
  x = Math.abs(x);
  y = Math.abs(y);
  while(y) {
    var t = y;
    y = x % y;
    x = t;
  }
  return x;
}

function generateGridSVG(width, height, segment){
  var output = "";

  for(var x = segment; x < width; x += segment){
    output += "M "+x+" 0 V "+height+" ";
  }

  for(var y = segment; y < height; y += segment){
    output += "M 0 "+y+" H "+width+" ";
  }

  return output;
}

function leadingZeros(num, len=2){
  num = String(num);
  return "0".repeat((len - num.length)) + num;
}
