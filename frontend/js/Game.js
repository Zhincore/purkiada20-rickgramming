class Game{
  constructor(level, onWin=()=>{}){
    this.level = level;
    this.field = this.level.field;
    this.playerPos = null;
    this.holePos = null;
    this.portals = {};
    this.obstacles = this.genMap();
    this.fieldSize = this.level.field.length;
    this.onWinCB = onWin;
    this.assets = this.parseAssets();
    this.explosion = this.assets.explosion;

    this.fieldRatio = 3/2;
    this.viewport = $("#viewport");
    this.procedureLimit = 5;
    this.fxSpeed = "slow";
    this.instructions = ["up", "down", "left", "right"];

    if(!this.playerPos || !this.holePos) return console.error("Invalid level.");

    this.program = [];
    this.procedures = [[]];
    this.moves = 0;
    this.posHistory = [];
    this.running = false;
    this.won = false;
    this.currentPos = [...this.playerPos];

    this.render();
    this.initEvents();

    /////
    this.explosion.find("video").get(0).load();
  }

  initEvents(){
    $("#control-signal-start").click(this.runProgram.bind(this));
    $("#control-signal-stop").click(this.reset.bind(this));

    return;
  }

  destroy(){
    $("#control-signal-start").off("click", this.runProgram.bind(this));
    $("#control-signal-stop").off("click", this.reset.bind(this));
    $("#viewport *").off();
    this.level = null;
    this.onWin = null;
    this.viewport = null;
  }

  onWin(){
    var height = this.viewport.height();
    var segment = height / this.fieldSize;
    var $player = $("#view-player");
    $player.fadeOut();

    $player.queue(()=>{
      this.onWinCB();
      $player.finish();
      $player.queue("fx", []);
      this.destroy();
    });

    return;
  }

  onFail(){
    var height = this.viewport.height();
    var segment = height / this.fieldSize;
    var $player = $("#view-player");

    ///// Generate explosion
    var explode = ()=>{
      var asset = this.assets["explosion"].clone();

      asset
        .css("top", segment * this.currentPos[1])
        .css("left", segment * this.currentPos[0])
        .width(segment)
        .height(segment)
      ;

      // Scale
      asset.find(".view-asset-big").width(segment).height(segment);

      this.viewport.append(asset);

      var video = asset.find("video");
      var media = video.get(0)

      return new Promise((resolve)=>{
        video.on("loadeddata", ()=>{
          $player.hide();
          media.play();

          video.on("ended", ()=>{
            resolve();
          })
        });

      });
    }

    $player.animate({
      top: segment * this.currentPos[1],
      left: segment * this.currentPos[0]
    }, this.fxSpeed);

    $player.queue((next)=>{
      explode().then(()=>{
        this.reset();
      });
    });

    return false;
  }

  reset(){
    var $player = $("#view-player");
    $player.finish();
    this.currentPos = [...this.playerPos];
    this.running = false;
    this.generateField();
    $(".control-node").removeClass("called");
  }

  runProgram(){
    if(this.running) return;
    this.running = true;
    this.moves = 0;
    this.posHistory = [];

    var height = this.viewport.height();
    var segment = height / this.fieldSize;
    var $player = $("#view-player");

    var _updatePos = (teleport=false)=>{
      var pos = [...this.currentPos];
      if(teleport){
        $player.slideUp("fast");
        $player.queue((next)=>{
          $player
            .css("top", segment * pos[1])
            .css("left", segment * pos[0])
          ;
          next();
        });
        $player.slideDown("fast");

      }else{
        $player.animate({
          top: segment * pos[1],
          left: segment * pos[0]
        }, this.fxSpeed);
      }
    }

    var _translateInst = (instr)=>{
      switch (instr) {
        case "up":
          if(this.currentPos[1] > 0){
            this.currentPos[1]--;
            _updatePos();
          }else{
            this.currentPos[1] -= 0.5;
            return this.onFail();
          }
          break;

        case "right":
          if(this.currentPos[0] < this.fieldSize-1){
            this.currentPos[0]++;
            _updatePos();
          }else{
            this.currentPos[0] += 0.5;
            return this.onFail();
          }
          break;

        case "down":
          if(this.currentPos[1] < this.fieldSize-1){
            this.currentPos[1]++;
            _updatePos();
          }else{
            this.currentPos[1] += 0.5;
            return this.onFail();
          }
          break;

        case "left":
          if(this.currentPos[0] > 0){
            this.currentPos[0]--;
            _updatePos();
          }else{
            this.currentPos[0] -= 0.5;
            return this.onFail();
          }
          break;
      }

      // Obstacles
      if(this.obstacles[this.currentPos[1]][this.currentPos[0]]){
        return this.onFail();

      // Portals
    }else if(this.field[this.currentPos[1]][this.currentPos[0]].startsWith("portal")){
        var portal_id = this.field[this.currentPos[1]][this.currentPos[0]].replace("portal_", "");
        var dest = null;
        this.portals[portal_id].some((portalPos)=>{
          if(portalPos[0] != this.currentPos[0] || portalPos[1] != this.currentPos[1]){
            dest = portalPos;
            return true;
          }
          return false;
        });

        if(dest){
          this.currentPos[0] = dest[0];
          this.currentPos[1] = dest[1];
          _updatePos(true);
        }

      // Worm hole
      }else if(this.currentPos[0] == this.holePos[0] && this.currentPos[1] == this.holePos[1]){
        this.won = true;
        //return false;
      }

      return true;
    }

    var _doProc = (proc, procI)=>{
      proc.every((instr, i)=>{
        $player.queue((next)=>{
          $(".control-procedure:eq("+procI+") .control-node:eq("+i+")").addClass("called");
          next();
        });

        if(!doInstr(instr)) return false;

        $player.queue((next)=>{
          $(".control-procedure:eq("+procI+") .control-node:eq("+i+")").removeClass("called");
          next();
        });

        return true;
      });
      return true;
    };

    var doInstr = (instr)=>{
      this.won = false;

      if(instr.startsWith("p")){
        var subprocI = Number(instr.replace("p", ""));
        var subproc = this.procedures[subprocI];

        if(!_doProc(subproc, subprocI)) return false;

      }else{
        if(!_translateInst(instr)) return false;
        this.moves++;
        this.posHistory.push([...this.currentPos]);
      }

      return true;
    };


    this.program.every((instr, i)=>{
      $player.queue((next)=>{
        $("#control-program .control-node:eq("+i+")").addClass("called");
        next();
      });

      if(!doInstr(instr)) return false;

      $player.queue((next)=>{
        $("#control-program .control-node:eq("+i+")").removeClass("called");
        next();
      });
      return true;
    });

    if(this.won){
      return this.onWin();
    }

    $player.queue((next)=>{
      setTimeout(()=>{
        this.reset();
      }, 500);
    });
    return;
  }

  parseAssets(){
    var output = {};

    Object.keys(ASSETS).forEach((key)=>{
      var id = "";

      if(key == "player"){
        id = "view-player";
      } else if(key == "worm_hole") {
        id = "view-worm_hole";
      }

      output[key] = $(`<div id="${id}" class="view-asset-wrap"></div>`).append(ASSETS[key]);
    });

    return output;
  }

  genMap(){
    var output = [];

    this.field.forEach((row, y)=>{
      output.push([]);
      row.forEach((col, x)=>{
        if(col.startsWith("obstacle")){
          return output[y].push(true);

        }else if(col == "player"){
          this.playerPos = [x, y];

        }else if(col == "worm_hole"){
          this.holePos = [x, y];

        }else if(col.startsWith("portal")){
          var portal_id = col.replace("portal_", "");
          if(portal_id in this.portals){
            this.portals[portal_id].push([x,y]);
          }else{
            this.portals[portal_id] = [[x,y]];
          }
        }
        output[y].push(false);
      });
    });

    return output;
  }

  render(){
    this.generateField();
    this.generateControls();
  }

  removeInstruction(origin, index){
    if(origin == "program"){
      this.program.splice(index, 1);
    }else if(origin.startsWith("procedure")){
      this.procedures[Number(origin.replace("procedure", ""))].splice(index, 1);
    }
    return;
  }


  generateField(){
    var width = this.viewport.width();
    var height = this.viewport.height();
    var segment = height / this.fieldSize;

    ///// Clear
    this.viewport.html(`<div class="panel-title">VIEWPORT</div><div class="border-skew-r"></div>`);

    ///// Grid
    var grid = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    grid.setAttribute("id", "viewport-grid");
    grid.setAttribute("width", width);
    grid.setAttribute("height", height);
    grid.setAttribute("xmlns", "http://www.w3.org/2000/svg");

    var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
    path.setAttribute("d", generateGridSVG(width, height, segment));
    //path.setAttribute("stroke", "white");

    grid.appendChild(path);

    this.viewport.append(grid);

    ///// Assets
    var obstacleCount = 1;
    this.field.forEach((row, y)=>{
      row.forEach((col, x)=>{
        if(!col || col == "") return;

        if(col.startsWith("portal")){
          var portal_id = col.replace("portal_", "");
          col = "portal";
        }

        if(col in this.assets){
          var asset = this.assets[col].clone();

          asset
            .css("top", segment * y)
            .css("left", segment * x)
            .width(segment)
            .height(segment)
          ;

          if(portal_id) asset.find(".view-asset-portal").attr("data-portal-id", portal_id);

          // Scale
          if(asset.find(".view-asset").length != 0){
            asset.find(".view-asset").width(segment/3*2).height(segment/3*2);
          }else if(asset.find(".view-asset-big").length != 0){
            asset.find(".view-asset-big").width(segment/5*4).height(segment/5*4);
          }else if(asset.find(".view-asset-small").length != 0){
            asset.find(".view-asset-small").width(segment/2).height(segment/2);
          }

          asset.attr("title", col+(portal_id ? "_"+portal_id : ""));

          if(this.obstacles[y][x]){
            asset.children().css("animation-delay", (-2 * (obstacleCount++))+"s");
          }

          this.viewport.append(asset);
        }
      });
    });

    return;
  }

  genNode (item) {
    return `<div class="control-node ${item}" data-instruction="${item}" draggable="true"></div>`;
  }

  generateControls(){

    $("#control-inner").html(`
      <h3>Instructions</h3>
      <div id="control-instructions" class="control-callstack"></div>

      <h3>Program</h3>
      <div id="control-program" ondragover="return false" class="control-callstack"></div>
    `);

    if(this.level.limit){
      $("#control-inner").append(`<div class="control-subtext">Instruction limit: ${this.level.limit}</div>`);
    }

    //// Instructions
    this.instructions.forEach((item)=>{
      $("#control-instructions").append(this.genNode(item));
    });

    this.procedures = [];
    for(var i = 0; i < this.level.procedures; i++){
      $("#control-instructions").append(`<div class="control-node" data-instruction="p${i}" draggable="true"></div>`);
      $("#control-inner").append(`<h3>Procedure ${i}</h3><div class="control-procedure control-callstack" ondragover="return false" data-procedure="${i}"></div>`);
      this.procedures.push([]);
    }

    $("#control-instructions").append(`<div style="clear:both"></div>`);


    //// Hint
    if(this.level.hint){
      $("#control-inner").append(`<h3>Hint</h3><div id="control-hint"></div>`);
      $("#control-hint").text(this.level.hint);
    }


    //// Events
    $(".control-node").on("dragstart", (ev)=>{
      ev.originalEvent.dataTransfer.setData("text", $(ev.currentTarget).attr("data-instruction"));
      ev.originalEvent.dataTransfer.dropEffect = "copy";
      ev.originalEvent.dataTransfer.effectAllowed = "copy";
    });

    $(".control-callstack").on("drop", (ev)=>{
      var $target = $(ev.currentTarget);
      var data = ev.originalEvent.dataTransfer.getData("text").split(";");

      ev.preventDefault();

      if(data == "") return;

      if($target.is("#control-program")){
        if(!this.level.limit || this.program.length < this.level.limit){
          this.program.push(data[0]);
        }
        if(data.length > 1){
          this.removeInstruction(data[2], data[1]);
        }

      }else if($target.is(".control-procedure")){
        var proc = this.procedures[Number($target.attr("data-procedure"))];

        if(proc.length < this.procedureLimit){
          proc.push(data[0]);
        }
        if(data.length > 1){
          this.removeInstruction(data[2], data[1]);
        }
      }

      this.generateCallStacks();
    });

    this.generateCallStacks();
    return;
  }

  generateCallStacks(){
    ///// Program
    $("#control-program").html("");

    this.program.forEach((item, i)=>{
      var node = $(this.genNode(item));
      node
        .addClass("active")
        .attr("data-index", i)
        .attr("data-origin", "program")
      ;

      $("#control-program").append(node);
    });

    $("#control-program").append(`<div style="clear:both"></div>`);


    ///// Procedures
    $(".control-procedure").html("");

    this.procedures.forEach((procedure, procI)=>{
      var $proc = $(".control-procedure:eq("+procI+")");

      procedure.forEach((item, i)=>{
        var node = $(this.genNode(item));
        node
          .addClass("active")
          .attr("data-index", i)
          .attr("data-origin", "procedure"+procI)
        ;

        $proc.append(node);
      });

      $proc.append(`<div style="clear:both"></div>`);
    });


    ///// Events
    $(".control-node.active").off("dragstart").on("dragstart", (ev)=>{
      ev.originalEvent.dataTransfer.setData("text",
        $(ev.currentTarget).attr("data-instruction")+
        ";"+
        $(ev.currentTarget).attr("data-index")+
        ";"+
        $(ev.currentTarget).attr("data-origin")
      );
      ev.originalEvent.dataTransfer.dropEffect = "move";
      ev.originalEvent.dataTransfer.effectAllowed = "move";
    });
    $(".control-node.active").on("dragend", (ev)=>{
      var index = $(ev.currentTarget).attr("data-index");
      var origin = $(ev.currentTarget).attr("data-origin");
      ev.preventDefault();
      ev.stopPropagation();

      this.removeInstruction(origin, index);
      this.generateCallStacks();
    });

    return;
  }
}
